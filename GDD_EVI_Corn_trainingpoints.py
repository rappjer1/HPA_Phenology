# -*- coding: utf-8 -*-
"""
Created on Sun Oct 01 17:36:37 2017

@author: Jeremy
"""

import pandas as pd
import matplotlib.pyplot as plot
import seaborn as sns
import numpy as np
import sklearn
from sklearn.linear_model import LinearRegression
from sklearn import cross_validation
from sklearn.cross_validation import train_test_split
from sklearn.cluster import KMeans 

from scipy.stats import linregress

GEE = pd.read_csv("file:///S:/Users/rappjer1/Scripts/2010_GDD_hourly_EVI_corn_training_points.csv")


#EVI Work (same as GDD Work for notes)
EVI_columns = [col for col in GEE.columns if 'EVI'in col or 'system:index' or 'lat' or 'long' 'masterType' or 'masterNum'in col]

EVI = GEE[EVI_columns]
EVI_only = [col for col in EVI.columns if 'EVI' in col]
melt_EVI = pd.melt(EVI, id_vars = ['system:index', 'lat', 'long','masterType','masterNum'], value_vars = EVI_only, var_name = 'EVI_index')
melt_columns_EVI = melt_EVI.columns

value_edit_EVI = []
for column in melt_columns_EVI:
    if 'value' in str(column):
        column = 'value_EVI'
        value_edit_EVI.append(column)
    elif 'system:index' in str(column):
        column = 'system:index_EVI'
        value_edit_EVI.append(column)
    else:
        value_edit_EVI.append(column)
melt_EVI.columns = value_edit_EVI

#GDD work, splits from GEE dataframe
GDD_columns = [col for col in GEE.columns if 'GDD' in col or 'system:index' or 'lat' or 'long' or 'masterType' or 'masterNum' in col] #new columns list
GDD = GEE[GDD_columns] #new dataframe of just GDD columns
GDD_only = [col for col in GDD.columns if 'GDD' in col]
melt_GDD = pd.melt(GDD, id_vars = ['system:index', 'lat', 'long', 'masterType','masterNum'], value_vars = GDD_only, var_name = 'GDD_index') #melt dataframe on systems index
melt_columns_GDD = melt_GDD.columns #columns from melt

value_edit_GDD = [] #edit column to avoid reduncandy in final dataframe
for column in melt_columns_GDD:
    if 'value' in str(column): #could change melt to output specific variable name, would make more sense as function for crop
        column = 'value_GDD'
        value_edit_GDD.append(column)
    else:
        value_edit_GDD.append(column)
melt_GDD.columns = value_edit_GDD #change columns to new names
        
#merge the EVI and GDD tables
merged = pd.concat([melt_EVI,melt_GDD], axis = 1)
#create day column
merged['EVI_index']=merged['EVI_index'].astype(str)
merged['Day'] = merged.EVI_index.apply(lambda x: int(x[-3:]))
#filter and create natural log of EVI column
merged = merged.drop(merged[(merged['value_EVI'] > .2) & (merged['value_GDD'] < 1500)].index.values, axis = 0)
merged['Ln_EVI'] = merged.value_EVI.apply(lambda x: np.log(x))
merged = merged.sort_values(['system:index','Day'], ascending=[True,True])


#filter the GDD to the greening up phase (some literature may be needed to correct for each crop)
#merged_filtered = merged[(merged['value_GDD'] > 1800) & (merged['value_GDD'] < 3000)]



#Graph filtered data of Ln EVI vs GDD's
#graph = sns.jointplot(x=merged.value_GDD, y=merged.value_EVI, marker='x', color='b', kind="scatter", alpha =.3)

#groupby work

merged_count= merged.groupby('system:index').count()
merged_low_count = merged_count.drop(merged_count[merged_count['value_EVI'].lt(10)].index.values, axis = 0)
desired_points = list(merged_low_count.index.values)

merged_desired = merged[merged['system:index'].isin(desired_points)]
merged_filtered = merged_desired[(merged_desired['value_GDD'] < 15000) & (merged_desired['value_GDD'] > 0)]
merged_count_filtered = merged_filtered.groupby('system:index').count()
merged_applied = merged_filtered.groupby('system:index').count().drop(merged_count_filtered.sample(frac=0, replace = True).index.values, axis = 0)
#merged_applied_low = merged_applied[merged_applied['value_EVI'].gt(55)]
desired_sampled_points = list(merged_applied.index.values)
merged_desired_sampled = merged_filtered[merged_filtered['system:index'].isin(desired_sampled_points)].reset_index().drop('index',1)
merged_desired_sampled = merged_desired_sampled.dropna(axis=0,how='any')
#####Linear Regression########
merged_desired_sampled = merged_desired_sampled[['system:index','value_GDD','Ln_EVI','value_EVI','lat','long','masterNum','masterType','Day']]
merged_desired_sampled.to_csv(path_or_buf='EVI_Corn_GDD_SHP_training_points_nontruncated_2.csv')

merged_slopes = merged_desired_sampled.groupby('system:index').apply(lambda x: linregress(x.value_GDD, x.Ln_EVI)[0])
merged_slopes = merged_slopes.dropna(axis=0,how='any')
merged_slopes = merged_slopes.reshape(-1,1)

merged_desired = merged_desired.loc[:,~merged_desired.columns.duplicated()]


#kmeans = KMeans(n_clusters=3, random_state=0).fit(merged_slopes)

#sample_mds_train, sample_mds_test = sklearn.cross_validation.train_test_split(merged_slopes, train_size=0.6)
    
#cluster = sklearn.cluster.KMeans(n_clusters=3, init='k-means++', n_init=10, max_iter=300,tol=0.0001, precompute_distances='auto', verbose=0, random_state=None, copy_x=True, n_jobs=1)
#cluster.fit(sample_mds_train)
#result = cluster.predict(sample_mds_test)
#
#fig, ax = plt.subplots(figsize=(12,8))
#fig2, ax2 = plt.subplots(figsize=(12,8))
#graph_points = merged_desired.plot(kind='scatter', x='value_GDD', y = 'Ln_EVI', ax = ax, alpha =.4, legend = False)
#for title, group in merged_desired_sampled.groupby('system:index'):
#    group.plot(kind ='line', x ='value_GDD', y ='Ln_EVI', c = 'grey', ax = ax2, alpha =.3, legend = False)

g=sns.lmplot(data=merged_desired, x='value_GDD',y='Ln_EVI',hue='masterType',fit_reg=False)

plot.rcParams['pdf.fonttype'] = 42 #truetype fonts
plot.rcParams['figure.autolayout'] = True
g.savefig('S:/Users/rappjer1/AGU 2017/Figure_scripts/Precip_figure.pdf', format='pdf', dpi=1000)

